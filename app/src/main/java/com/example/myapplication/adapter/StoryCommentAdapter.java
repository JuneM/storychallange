package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.databinding.AdapterCommentBinding;
import com.example.myapplication.repository.remote.model.CommentModel;

import java.util.List;

public class StoryCommentAdapter extends RecyclerView.Adapter<StoryCommentAdapter.CommentViewHolder> {

    private Context context;
    private List<CommentModel> mStoryCommentList;

    public StoryCommentAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AdapterCommentBinding adapterBinding = DataBindingUtil.inflate(inflater, R.layout.adapter_comment, parent, false);
        return new CommentViewHolder(adapterBinding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        if (mStoryCommentList != null) {
            CommentModel data = mStoryCommentList.get(position);
            holder.bind(data);
        }
    }

    public void setComment(List<CommentModel> storyList) {
        mStoryCommentList = storyList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mStoryCommentList != null){
            return mStoryCommentList.size();
        }else {
            return 0;
        }
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private AdapterCommentBinding binding;

        public CommentViewHolder(AdapterCommentBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(CommentModel data) {

            binding.tvNameCommment.setText(data.getBy());
            if (data.getText() != null){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.tvIsiComment.setText(Html.fromHtml(data.getText(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    binding.tvIsiComment.setText(Html.fromHtml(data.getText()));
                }
            }
        }
    }
}
