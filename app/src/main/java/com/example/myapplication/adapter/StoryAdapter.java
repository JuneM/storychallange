package com.example.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.databinding.AdapterStoryBinding;
import com.example.myapplication.repository.remote.model.StoryModel;
import com.example.myapplication.ui.DetailStoryActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.StoryViewHolder> {

    private Context context;
    private List<StoryModel> mStoryList;

    public StoryAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AdapterStoryBinding adapterBinding = DataBindingUtil.inflate(inflater, R.layout.adapter_story, parent, false);
        return new StoryViewHolder(adapterBinding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryViewHolder holder, int position) {
        if (mStoryList != null) {
            StoryModel data = mStoryList.get(position);
            holder.bind(data);
        }
    }

    public void setStory(List<StoryModel> storyList) {
        mStoryList = storyList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mStoryList != null){
            return mStoryList.size();
        }else {
            return 0;
        }
    }

    public class StoryViewHolder extends RecyclerView.ViewHolder {

        AdapterStoryBinding binding;
        Context context;

        public StoryViewHolder(AdapterStoryBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        void bind(StoryModel data){
            binding.tvTitle.setText(data.getTitle());
            binding.tvType.setText(data.getType());
            binding.tvScore.setText("Score : " + data.getScore());
            if (data.getKids()!= null){
                binding.tvComment.setText("Comment : " + data.getKids().size());
            }else{
                binding.tvComment.setText("Comment : 0");
            }
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy' 'HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.getTime());
            binding.tvDate.setText(sdf.format(calendar.getTime()));

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, DetailStoryActivity.class);
                intent.putExtra("title",data.getTitle());
                intent.putExtra("time",sdf.format(calendar.getTime()));
                intent.putExtra("by",data.getBy());
                intent.putIntegerArrayListExtra("kids", data.getKids());
                context.startActivity(intent);
            });
        }
    }
}
