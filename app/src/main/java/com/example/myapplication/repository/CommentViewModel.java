package com.example.myapplication.repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.repository.remote.StoryRepository;
import com.example.myapplication.repository.remote.model.CommentModel;

import java.util.ArrayList;
import java.util.List;

public class CommentViewModel extends AndroidViewModel {

    private MutableLiveData<List<CommentModel>> mAllComment;
    private StoryRepository storyRepository;

    public CommentViewModel(@NonNull Application application) {
        super(application);
        storyRepository = new StoryRepository();
    }

    public LiveData<List<CommentModel>> getAllComment(ArrayList<Integer> listCommentId) {
        mAllComment = storyRepository.getAllComment(listCommentId);
        return mAllComment;
    }

}
