package com.example.myapplication.repository.remote;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.repository.remote.model.CommentModel;
import com.example.myapplication.repository.remote.model.StoryModel;
import com.example.myapplication.repository.remote.retrofit.GetServices;
import com.example.myapplication.repository.remote.retrofit.RetrofitClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoryRepository {
    private static StoryRepository storyRepository;

    public static StoryRepository getInstance(){
        if (storyRepository == null){
            storyRepository = new StoryRepository();
        }
        return storyRepository;
    }

    private GetServices apiService;

    public StoryRepository() {
        apiService = RetrofitClient.getRetrofitInstance().create(GetServices.class);
    }

    public MutableLiveData<List<StoryModel>> getAllStory() {
        MutableLiveData<List<StoryModel>> mutableLiveData = new MutableLiveData<>();
        apiService.getAllTopStoryId().enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                List<Integer> topStories = response.body();
                ArrayList<StoryModel> tempStoryList = new ArrayList<>();
                for (int i = 0; i < topStories.size(); i++) {
                    apiService.getArticle(topStories.get(i)).enqueue(new Callback<StoryModel>() {
                        @Override
                        public void onResponse(Call<StoryModel> call, Response<StoryModel> response) {
                            tempStoryList.add(response.body());
                            mutableLiveData.setValue(tempStoryList);
                        }

                        @Override
                        public void onFailure(Call<StoryModel> call, Throwable t) {
                            mutableLiveData.setValue(null);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Integer>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }

    public MutableLiveData<List<CommentModel>> getAllComment(ArrayList<Integer> listIdComment){
        Log.d("koneksi", "getAllComment: masuk");
        MutableLiveData<List<CommentModel>> mutableLiveData = new MutableLiveData<>();
        ArrayList<CommentModel> tempCommentList = new ArrayList<>();
        for (int i = 0; i < listIdComment.size(); i++) {
            apiService.getComment(listIdComment.get(i)).enqueue(new Callback<CommentModel>() {
                @Override
                public void onResponse(Call<CommentModel> call, Response<CommentModel> response) {
                    Log.d("koneksi", "getAllComment: masuk");
                    tempCommentList.add(response.body());
                    mutableLiveData.setValue(tempCommentList);
                }

                @Override
                public void onFailure(Call<CommentModel> call, Throwable t) {
                    Log.d("koneksi", "getAllComment: gagal");
                    t.printStackTrace();
                    mutableLiveData.setValue(null);
                }
            });
        }

        return mutableLiveData;
    }

}
