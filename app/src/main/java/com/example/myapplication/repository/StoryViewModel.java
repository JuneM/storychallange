package com.example.myapplication.repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.repository.remote.StoryRepository;
import com.example.myapplication.repository.remote.model.StoryModel;

import java.util.List;

public class StoryViewModel extends AndroidViewModel {

    private MutableLiveData<List<StoryModel>> mAllStory;
    private StoryRepository storyRepository;

    public StoryViewModel(@NonNull Application application) {
        super(application);
        storyRepository = new StoryRepository();
        mAllStory = storyRepository.getAllStory();
    }


    public LiveData<List<StoryModel>> getAllStory() {
        return mAllStory;
    }
}
