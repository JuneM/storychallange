package com.example.myapplication.repository.remote.retrofit;

import com.example.myapplication.repository.remote.model.CommentModel;
import com.example.myapplication.repository.remote.model.StoryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetServices {
    @GET("topstories.json?print=pretty")
    Call<List<Integer>> getAllTopStoryId();

    @GET("item/{articleid}.json?print=pretty")
    Call<StoryModel> getArticle(@Path("articleid") int id);

    @GET("item/{articleid}.json?print=pretty")
    Call<CommentModel> getComment(@Path("articleid") int id);

}
