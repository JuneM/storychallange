package com.example.myapplication.ui;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.myapplication.BaseActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.StoryAdapter;
import com.example.myapplication.databinding.ActivityMainBinding;
import com.example.myapplication.repository.StoryViewModel;

public class MainActivity extends BaseActivity {

    private MainActivity activity = this;
    private Context context = this;
    private ActivityMainBinding binding;

    private StoryAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private StoryViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_main);

        initRecyclerView();


        if (cekConnection()){
            mViewModel = new ViewModelProvider(this).get(StoryViewModel.class);
            mViewModel.getAllStory().observe(this, list -> {
                adapter.setStory(list);
            });
        }else{
            Toast.makeText(activity, "Periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
        }

    }

    private void initRecyclerView() {
        /*Init Layout Manager*/
        gridLayoutManager = new GridLayoutManager(context, 1);
        binding.rvNews.setLayoutManager(gridLayoutManager);
        adapter = new StoryAdapter(context);
        binding.rvNews.setAdapter(adapter);

    }
}
