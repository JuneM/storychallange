package com.example.myapplication.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.BaseActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.StoryAdapter;
import com.example.myapplication.adapter.StoryCommentAdapter;
import com.example.myapplication.databinding.ActivityDetailStoryBinding;
import com.example.myapplication.databinding.ActivityMainBinding;
import com.example.myapplication.repository.CommentViewModel;
import com.example.myapplication.repository.StoryViewModel;
import com.example.myapplication.repository.remote.model.CommentModel;
import com.example.myapplication.repository.remote.model.StoryModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DetailStoryActivity extends BaseActivity {

    private DetailStoryActivity activity = this;
    private Context context = this;
    private ActivityDetailStoryBinding binding;

    private StoryCommentAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private CommentViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_detail_story);

        initRecyclerView();
        getDataIntent();
        ArrayList<Integer> listIdComment = getIntent().getIntegerArrayListExtra("kids");
        if (cekConnection()){
            mViewModel = new ViewModelProvider(this).get(CommentViewModel.class);
            mViewModel.getAllComment(listIdComment).observe(this, list -> {
                adapter.setComment(list);
            });
        }else{
            Toast.makeText(activity, "Periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
        }
    }

    private void getDataIntent() {
        binding.tvTitle.setText(getIntent().getStringExtra("title"));
        binding.tvNameAuthor.setText("By : "+getIntent().getStringExtra("by"));
        binding.tvDate.setText(getIntent().getStringExtra("time"));
    }

    private void initRecyclerView() {
        /*Init Layout Manager*/
        gridLayoutManager = new GridLayoutManager(context, 1);
        binding.rvListComment.setLayoutManager(gridLayoutManager);
        adapter = new StoryCommentAdapter(context);
        binding.rvListComment.setAdapter(adapter);

    }
}
